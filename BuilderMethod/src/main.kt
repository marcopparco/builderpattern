
interface Builder {
    fun setNumPuertas(puertas: Int): Int
    fun setNumAsientos(asientos: Int): Int
    fun setMotor(motor: String): String
    fun setGPS(gps: Boolean): Boolean


}


class CarBuilder : Builder {

    var numPuertas: Int = 0
    var numAsientos: Int = 0
    var motor: String = ""
    var gps: Boolean = false

    override fun setNumPuertas(puertas: Int): Int {
        this.numPuertas = puertas
        return numPuertas
    }

    override fun setNumAsientos(asientos: Int): Int {
        this.numAsientos = asientos
        return numAsientos
    }

    override fun setMotor(motor: String): String {
        this.motor = motor
        return this.motor
    }

    override fun setGPS(gps: Boolean): Boolean {
        this.gps = gps
        return this.gps
    }

    fun getResult(): Car {
        var car= Car(numPuertas,numAsientos,motor,gps)
        return car
    }

}



class CarManualBuilder : Builder {

    var numPuertas: Int = 0
    var numAsientos: Int = 0
    var motor: String = ""
    var gps: Boolean = false

    override fun setNumPuertas(puertas: Int): Int {
        this.numPuertas = puertas
        return numPuertas
    }

    override fun setNumAsientos(asientos: Int): Int {
        this.numAsientos = asientos
        return numAsientos
    }

    override fun setMotor(motor: String): String {
        this.motor = motor
        return this.motor
    }

    override fun setGPS(gps: Boolean): Boolean {
        this.gps = gps
        return this.gps
    }
    fun getResult(): Manual {
        var manual= Manual(numPuertas,numAsientos,motor,gps)
        return manual
    }

}


class Manual(numPuertas: Int, numAsientos: Int, motor: String, gps: Boolean) {
    var numPuertas: Int = numPuertas
    var numAsientos: Int = numAsientos
    var motor: String = motor
    var gps: Boolean = gps

    fun imprimir(): String {
        var info: String = ""
        info += "El vehiculo cuenta con: $numPuertas puertas,\n"
        info += "El vehiculo cuenta con: $numAsientos asientos ,\n"
        info += "El motor del vehiculo es : $motor,\n"
        if (gps != false) {
            info += "El vehiculo cuenta con Navegacion GPS\n"
        } else {
            info += "El vehiculo NO cuenta con Navegacion GPS\n"
        }
        return info
    }
}

class Car (numPuertas: Int, numAsientos: Int, motor: String, gps: Boolean) {
    var numPuertas: Int =numPuertas
        set(value) {
            this.numPuertas=value
        }

    var numAsientos: Int=numAsientos
        set(value) {
            this.numAsientos=value
        }

    var motor: String = motor
        set(value:String) {
            this.motor=value
        }

    var gps: Boolean = gps
        set(value:Boolean) {
            this.gps=value
        }


}



class Director {

    fun construirCarroDeportivo(builder : Builder){
        builder.setNumPuertas(5)
        builder.setNumAsientos(4)
        builder.setGPS(true)
        builder.setMotor("HYBRID-250")
    }

    fun construirCarroFamiliar(builder : Builder){
        builder.setNumPuertas(5)
        builder.setNumAsientos(8)
        builder.setGPS(false)
        builder.setMotor("DIESEL-1000")
    }

    fun construirSUV(builder : Builder){
        builder.setNumPuertas(5)
        builder.setNumAsientos(8)
        builder.setGPS(false)
        builder.setMotor("DIESEL-1000")
    }


}



    fun main(args: Array<String>) {
        var director = Director();

        var carBuilder= CarBuilder();
        director.construirCarroDeportivo(carBuilder)

        var car=carBuilder.getResult()
        println("EL tipo de carro construido es: \n" + car.motor )
        var manualBuilder= CarManualBuilder()
        director.construirCarroDeportivo(manualBuilder);
        var carManual=manualBuilder.getResult()
        println("\n Manual de Construccion : \n {${carManual.imprimir()}}")



    }

